<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Private routes
$router->group(['middleware' => 'auth'], function () use ($router) {
  $router->get('/who-am-i', 'AuthController@whoAmI');
  $router->post('/check-access', 'AuthController@checkAccess');
  $router->group(['prefix' => 'user'], function () use ($router) {
    $router->post("/", "UserController@store");
  });
});

$router->post('/login', 'AuthController@login');
$router->get('/key', function () {
  return \Illuminate\Support\Str::random(32);
});
