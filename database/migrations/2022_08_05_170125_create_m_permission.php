<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_permission', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('id_parent')->nullable();
            $table->string("perm_name");
            $table->string("perm_key")->unique('perm_key_unique');
            $table->boolean("is_main");
            $table->timestampsTz();
        });

        Schema::table('m_permission', function (Blueprint $table) {
            $table->foreign('id_parent')->references("id")->on("m_permission");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_permission');
    }
};
