<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_sidebar', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->integer('order');
            $table->string('url')->nullable();
            $table->string('icon')->nullable();
            $table->string('permission_key')->nullable();
            $table->timestampsTz();
        });
        Schema::table('m_sidebar', function (Blueprint $table) {
            $table->foreign('permission_key')->references("perm_key")->on("m_permission");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_sidebar');
    }
};
