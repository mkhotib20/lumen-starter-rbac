<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_access', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('id_role')->nullable();
            $table->string('access_key');
            $table->timestampsTz();
        });
        Schema::table('auth_access', function (Blueprint $table) {
            $table->foreign('id_role')->references("id")->on("auth_role");
        });
        Schema::table('auth_access', function (Blueprint $table) {
            $table->foreign('access_key')->references("perm_key")->on("m_permission");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_access');
    }
};
