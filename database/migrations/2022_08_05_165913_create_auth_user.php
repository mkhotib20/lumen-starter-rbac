<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_user', function (Blueprint $table) {
            $table->id();
            $table->string("full_name");
            $table->string("email")->unique("email_unique");
            $table->string("password");
            $table->unsignedInteger('id_role')->nullable();
            $table->boolean("is_readonly")->default(false);
            $table->timestampsTz();
            $table->softDeletes();
        });
        Schema::table('auth_user', function (Blueprint $table) {
            $table->foreign('id_role')->references("id")->on("auth_role");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_user');
    }
};
