<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createdId = DB::table('auth_role')->insertGetId([
            'role_name' => "Super Admin",
            'is_readonly' => true,
            'is_super' => true,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ]);

        DB::table('auth_user')->insert([
            'full_name' => "Administrator",
            'email' => 'admin@mail.com',
            'password' => Hash::make('Admin!23'),
            'is_readonly' => true,
            "id_role" => $createdId,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ]);
    }
}
