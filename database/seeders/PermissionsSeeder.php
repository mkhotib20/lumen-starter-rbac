<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("m_permission")->insert([
            [
                "perm_name" => "Create User",
                "perm_key" => "auth.user.create",
                "is_main" => false,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "perm_name" => "Show User",
                "perm_key" => "auth.user.show",
                "is_main" => true,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "perm_name" => "Update User",
                "perm_key" => "auth.user.update",
                "is_main" => false,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "perm_name" => "Delete User",
                "perm_key" => "auth.user.delete",
                "is_main" => false,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        ]);
    }
}
