<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class ServiceMakeCommand extends GeneratorCommand
{
  /**
   * The console command name.
   *
   * @var string
   */
  protected $name = 'make:service';
  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Create a new service class';
  /**
   * The type of class being generated.
   *
   * @var string
   */
  protected $type = 'Service';
  /**
   * Get the stub file for the generator.
   *
   * @return string
   */
  protected function getStub()
  {
    if ($this->option('resource')) {
      return __DIR__ . '/stubs/service.stub';
    }
    return __DIR__ . '/stubs/service.plain.stub';
  }
  /**
   * Get the default namespace for the class.
   *
   * @param  string  $rootNamespace
   * @return string
   */
  protected function getDefaultNamespace($rootNamespace)
  {
    return $rootNamespace . '\Services';
  }
  /**
   * Get the console command options.
   *
   * @return array
   */
  protected function getOptions()
  {
    return [
      ['resource', null, InputOption::VALUE_NONE, 'Generate a resource service class.'],
    ];
  }
}
