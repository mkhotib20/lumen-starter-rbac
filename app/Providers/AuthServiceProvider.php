<?php

namespace App\Providers;

use App\Models\Access;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Gate::define('access', function (User $user, $accessName) {
            $isSuper = $user->role()->first()->is_super;
            if ($isSuper) {
                return true;
            }
            $access = Access::where([
                "id_role" => $user->id_role,
                "access_key" => $accessName
            ])->count();
            if (!$access) abort(403, "You're not authorized to access this resource");
            return !!$access;
        });
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->input('api_token')) {
                return User::where('api_token', $request->input('api_token'))->first();
            }
        });
    }
}
