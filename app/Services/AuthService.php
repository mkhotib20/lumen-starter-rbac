<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthService
{
  private User $user;

  public function __construct(User $user)
  {
    $this->user = $user;
  }
  public function handleLogin($data)
  {
    $email = $data['email'];
    $password = $data['password'];
    $foundUser = $this->user->where("email", $email)->first();


    if (!$foundUser || !Hash::check($password, $foundUser->password)) {
      return null;
    }
    return $foundUser;
  }
}
