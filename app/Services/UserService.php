<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserService
{
  private User $user;

  public function __construct(User $user)
  {
    $this->user = $user;
  }
  public function store($payaload)
  {
    $data = $payaload;
    $data['password'] = Hash::make($payaload['password']);

    $saved = User::create($data);
    return $saved;
  }
}
