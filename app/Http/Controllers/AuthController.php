<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Access;
use App\Services\AuthService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
  public function login(Request $request, AuthService $authService)
  {
    $this->validate($request, [
      "email" => "required|email",
      "password" => "required",
    ]);
    $data = $authService->handleLogin($request->all());
    if (!$data) {
      return response(["status" => "401", "message" => "Bad credentials"], 401);
    }
    $token = Auth::login($data);
    return [
      "token" => $token
    ];
  }

  public function whoAmI()
  {
    return [
      "data" => Auth::user()
    ];
  }

  public function checkAccess(Request $request)
  {
    $this->validate($request, [
      "access" => "required|array",
    ]);
    $access_keys = $request->all()["access"];
    $user = $request->user();
    $is_super = $request->user()->role()->first()->is_super;

    $result = array_map(function ($item) use ($is_super, $user) {
      $has_access = Access::where([
        "access_key" => $item,
        "id_role" => $user->id_role
      ])->first();
      return [$item => !!$has_access  || $is_super];
    }, $access_keys);


    return ["data" => $result];
  }
}
