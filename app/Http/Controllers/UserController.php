<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
  public function store(Request $request, UserService $service)
  {
    Gate::check("access", "auth.user.create");
    $this->validate($request, [
      "email" => "required|email",
      "password" => "required",
      "full_name" => "required",
      "id_role" => "required|numeric",
    ]);

    $data = $service->store($request->all());
    return $data;
  }
}
